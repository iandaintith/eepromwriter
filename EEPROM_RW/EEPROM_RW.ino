/*******************************************************************
 DS2433 1-Wire Serial Programming Utility
 Ian Daintith
 iandaintith@gmail.com
 2nd June 2013 

 *******************************************************************/

#include "OneWire.h"

#define BYTES_IN_FILE_LINE   75// 72
#define LINES_IN_FILE        32
#define DATA_BYTES_PER_LINE  16

static const char ga_Hex[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' }; 
OneWire           ds(6);                 // Configure the OneWire bus on digital I/O pin 6
unsigned char     g_bBuffer[600];        // Buffer to use when reading and writing to device


//////////////////////////////////////////
// Called on boot up
void setup() 
{
  // Set up serial comms on 9600 8 N 1
  Serial.begin (9600);
  DisplayBanner(); 
  Serial.println(">Waiting for Device");
}


//////////////////////////////////////////
// Main loop
void loop() 
{
  // Clear out our buffers
  //
  memset(g_bBuffer, 0, 512);  
  
  ////////////////////////////////  
  // Wait until a device is present on the bus
  while(!ds.reset())
  {
    delay(100); 
  }
  
  ////////////////////////////////
  // OK we have a device, lets read the Family ID and Serial Number
  Serial.println(">Device found!");  
  Serial.println(">Reading Lasered ROM"); 
    
  ReadSerialNumber(g_bBuffer);
  DisplaySerialNumber(g_bBuffer); 
  DisplayUserInstruction(); 
 
  ////////////////////////////////
  // Get the user command [R or r] to READ ROM, [W or w] to WRITE ROM   
  do
  {    
    if(Serial.available() > 0)
    {
      int ch = Serial.read(); 
          
      if(ch == 'R' || ch =='r')
      {
        // we are doing a READ ROM 
        // 
        memset(g_bBuffer, 0, 512);        // clear buffer
        
        if(ReadRom(g_bBuffer, 0, 512))       // read device ROM
        {
          DisplayData(g_bBuffer, 512);      // send to terminal
          Serial.println(">Read ROM Complete OK"); 
        }
        else
        {
          Serial.println("!ERROR No device connected"); 
        }
        DisplayUserInstruction();
      }
      else if(ch == 'W' || ch == 'w')
      {
        Serial.println(">Write ROM selected, please send text file"); 
    
        // we are doing a WRITE ROM
        //
        memset(g_bBuffer, 0, 512);       // clear buffer

        if(ReadTextFile(g_bBuffer))      // read file from terminal
        {
          Serial.println(">File Read OK - data as follows"); 
          DisplayData(g_bBuffer, 512);   // send file to terminal
          
          Serial.println(">Writing ROM");    
          if(WriteDS2433Memory(g_bBuffer))  // program device
          {
            Serial.println(">Write ROM Complete OK");
          }
          else
          {
            Serial.println("!ERROR No device connected"); 
          } 
        }
        else
        {
           Serial.println("!ERROR file read unsuccessful"); 
        }
        
        // flush the incoming serial as the file may contain garbage after the data
        while(Serial.available() > 0)
        {
          char ch = Serial.read();
        }
        DisplayUserInstruction();
      }
    }
    delay(50); 
    
  } while(1); // forever loop
}

char szLine[80]; 
   
   

//////////////////////////////////////////////////////
// Populate the buffer with the 512 byte data read from the ASCII file
// Returns TRUE if we have extrated 32 lines of 16 Bytes (512 Bytes)
// Returns FALSE on error
boolean ReadTextFile(unsigned char *pBuffer)
{
  unsigned char *ptr = pBuffer;  
  int nBytesRead = 0; 
  int nTotalBytesRead = 0; 
  int nBytesToRead = 0;
  boolean blFirstData = TRUE; 
 
  // for every line in the file
  //
  for(int nLine = 0; nLine < LINES_IN_FILE; nLine++)
  {
    Serial.setTimeout(15000);     // 15 seconds
    
    nBytesToRead = BYTES_IN_FILE_LINE; 
    
    // if we are reading the last line we maynot have a \n\r pair
    if(nLine == (LINES_IN_FILE-1))
    {
      nBytesToRead -= 2; 
    }
    nBytesRead = Serial.readBytes(szLine, nBytesToRead); 
    
    if((nBytesRead > 0) && (blFirstData))
    {
      Serial.println(">Reading data file"); 
      blFirstData = FALSE; 
    }
    
    // heave we read the correct amount of bytes for this line?
    if(nBytesRead != nBytesToRead)
    {
      Serial.println();
      Serial.print("!ERROR found in line "); 
      Serial.print(nLine + 1); 
      Serial.println();
      break; 
    }
    else
    {
      Serial.print("."); 
      
      // we have a good line, add a null terminator at the end
      szLine[nBytesToRead] = 0;
      
      // extract the 16 bytes of data from the line
      for(int i=0; i<16; i++)
      {
        int nOffset = 8 + (i*3); 
        unsigned char nData; 
        char szTemp; 
        
        szTemp = szLine[nOffset++];
        szTemp = toupper(szTemp); 
        nData = ConvertHexCharToValue(szTemp); 
        nData <<= 4 ;
        szTemp = szLine[nOffset];  
        szTemp = toupper(szTemp); 
        nData += ConvertHexCharToValue(szTemp); 
        *ptr++ = nData; 
        
        // we have read a data byte
        nTotalBytesRead++; 
      }
    }
  }
  Serial.println(); 
  Serial.print("Data Bytes Read from file = "); 
  Serial.print(nTotalBytesRead);
  Serial.println(); 
 
  return (nTotalBytesRead == (LINES_IN_FILE * DATA_BYTES_PER_LINE));   
}


//////////////////////////////////////////////////////
// Display the formatted data on the terminal
// Dump buffer to terminal in format of 32 lines 
// of 16 bytes
void DisplayData(unsigned char *pBuffer, int nBytes)
{
  unsigned char *ptr = pBuffer; 
  unsigned int  nAddress = 0; 
  unsigned char szAsciiLine[17]; 
  char szTemp[16]; 
  int j, k;
      
  // formatted as 16 bytes wide and 32 lines long   
  for(j=0; j < (LINES_IN_FILE) ; j++)
  {
    sprintf(szTemp, "%06d: ", nAddress);
    Serial.print((const char*)szTemp); 
    
    // print the hex data
    for(k=0; k < 16; k++)
    {
      szAsciiLine[k] = *ptr;
      sprintf(szTemp, "%02X ", *ptr++); 
      Serial.print(szTemp);
      nAddress++; 
    }
    
    Serial.print("  ");     // to match the format required
       
    // print the ascii data (use a period character if non printable)    
    for(k=0; k < 16; k++)
    {  
      if((szAsciiLine[k] < 0x20) || (szAsciiLine[k] >= 0x7F))
      {
        szAsciiLine[k] = '.'; 
      }
    }
    szAsciiLine[16] = 0;  // add null
    Serial.println((const char*)szAsciiLine); 
  } 
}

//////////////////////////////////////////////////////
// Display the formatted Serial Number on the terminal
// pBuffer points to buffer containing 
// 1 byte Family code, 6 bytes ID, 1 byte CRC
void DisplaySerialNumber(unsigned char* pBuffer)
{
  unsigned char bNum;
 
  Serial.print(">Device Family = "); 
  Serial.print(pBuffer[0], HEX);
  Serial.println(); 
  Serial.print(">Serial Number = "); 
      
  for(int i=6; i>0; i--)
  {
    bNum = pBuffer[i];
    if(bNum < 0x10) 
    {
      Serial.print("0");  // add a leading zero where needed
    }
    Serial.print(bNum, HEX); 
  }
  Serial.println(); 
  Serial.print(">CRC = "); 
  bNum = pBuffer[7];
  if(bNum < 0x10) 
  {
    Serial.print("0");  // add a leading zero where needed
  }
  Serial.print(bNum, HEX); 
  Serial.println(); 
}

//////////////////////////////////////////////////////
// Write entire 512 Bytes (4096 bits) to DS2433
// Scratch Pad is 256 bits i.e) 32 Bytes
// So we need to write 32 bytes at a time, 16 times
// Returns false if no device
//////////////////////////////////////////////////////
boolean WriteDS2433Memory(const unsigned char *ptr)
{
  unsigned short nOffset = 0; 
  unsigned short lsb; 
  unsigned short msb; 
  unsigned char  ta1, ta2, es;
  unsigned char   bScratchPad[32]; 
    
  // there are 16 scrachpad writes of 32 bytes  
  for(int i=0; i < 16; i++)
  {
    lsb = 0x00FF & nOffset; 
    msb = 0xFF00 & nOffset;
    msb >>= 8;  
     
    if(!WriteScratchPad((unsigned char*) ptr, (unsigned char) lsb, (unsigned char) msb, 32))
      return FALSE; 
      
    if(!ReadScratchPad(bScratchPad, 32, &ta1, &ta2, &es))
      return FALSE;
    
    if(!CopyScratchPad(lsb, msb, es))
      return FALSE; 
    
    nOffset += 32; 
    ptr += 32; 
    Serial.print("."); 
  }
  Serial.println(); 
  return TRUE; 
}

//////////////////////////////////////////////////////
// DS2433 FUNCTIONS
//////////////////////////////////////////////////////
// Write to scratch pad
// Returns false if no device
boolean WriteScratchPad(unsigned char* ptr, unsigned char nAddressLSB, unsigned char nAddressMSB, int nBytes)
{
  unsigned char ucCommand[3] = {0x0F, 0x00, 0x00}; 
  boolean blret = FALSE;
  ucCommand[1] = nAddressLSB; 
  ucCommand[2] = nAddressMSB;
  
  if(blret = ds.reset())
  {  
    ds.skip();
    ds.write(ucCommand[0], 1);          
    ds.write(ucCommand[1], 1); 
    ds.write(ucCommand[2], 1);
      
    for(int i = 0; i < nBytes; i++) 
    {    
      ds.write(*ptr++, 1);
    }  
  }
  return blret; 
}

//////////////////////////////////////////////////////
// Read current contents of scratchpad
// Returns false if no device
boolean ReadScratchPad(unsigned char* ptr, int nBytes, unsigned char* ta1, unsigned char* ta2, unsigned char* es)
{
  unsigned char ucCommand; 
  boolean blret = FALSE; 
  
  ucCommand = 0xAA;  
  if(blret = ds.reset())
  {  
    ds.skip();
    ds.write(ucCommand, 1);          
    
    *ta1 = ds.read(); 
    *ta2 = ds.read(); 
    *es = ds.read(); 
     
    for(int i = 0; i < nBytes; i++) 
    {    
      *ptr++ = ds.read(); 
    }
  }   
}

//////////////////////////////////////////////////////
// Copy current contents of scratchpad to EEPROM
// Returns false if no device
boolean CopyScratchPad(unsigned char lsb, unsigned char msb, unsigned char es)
{
  unsigned char ucCommand[4];
  boolean  blret = FALSE; 
  
  ucCommand[0] = 0x55; 
  ucCommand[1] = lsb; 
  ucCommand[2] = msb; 
  ucCommand[3] = es; 
  
  if(blret = ds.reset())
  {  
    ds.skip();  
    ds.write(ucCommand[0], 1);           
    ds.write(ucCommand[1], 1); 
    ds.write(ucCommand[2], 1);           
    ds.write(ucCommand[3], 1); 
    delay(20);                     // wait at least 10m for tPROG time
  }
  return blret; 
}
//////////////////////////////////////////////////////
// Read Rom
// Returns false if no device
boolean ReadRom(unsigned char* ptr, unsigned char nOffsetAddress, int nBytes) 
{
  unsigned char ucCommand[3] = {0xF0, 0x00, 0x00}; 
  boolean  blret = FALSE; 
  ucCommand[1] = nOffsetAddress;         // start address 0
  ucCommand[2] = 0; 
 
  if(blret = ds.reset())
  {  
    ds.skip();
    ds.write(ucCommand[0],1);  
    ds.write(ucCommand[1],1);
    ds.write(ucCommand[2],1); 

    // read data back
    for(int i = 0; i < nBytes; i++) 
    {
      *ptr++ = ds.read();    
    }
  }
  return blret; 
}
//////////////////////////////////////////////////////
// Read Serial Number of DS 2433
// Returns false if no device
boolean ReadSerialNumber(unsigned char* ptr)
{
  boolean blret = FALSE; 
  unsigned char ucCommand = 0x33; 
  
  if(blret = ds.reset())
  {  
    ds.write(ucCommand, 1); // Send Command, leave ghost power on
     
    // Read the 1 byte Family Code, 6 bytes Serial Number and 1 byte CRC
    //
    for(int i = 0; i < 8; i++) 
    {
      *ptr++ = ds.read();               
    }
  }
  return blret; 
}


//////////////////////////////////////////////////////
// Display Interface Banner
void DisplayBanner(void)
{
  Serial.println("****************************************************"); 
  Serial.println("* MAXIM DS 2433 EEPROM Serial Utility              *"); 
  Serial.println("* Version 1.2  06/21/2013 iandaintith@gmail.com    *"); 
  Serial.println("****************************************************");   
}

//////////////////////////////////////////////////////
// Display User Instruction Banner
void DisplayUserInstruction(void)
{
  Serial.println(); 
  Serial.println("Enter [R/r] to Read ROM or [W/w] to Write ROM"); 
}

//////////////////////////////////////////////////////
// Ascii Hex to Value conversion
// Returns 0-15 if hex character supplied, -1 otherwise
unsigned char ConvertHexCharToValue(const char szHex)
{
  for(int i=0; i < 16; i++)
  {
    if(szHex == ga_Hex[i])
    {
        // we have found our match
        return (unsigned char) i; 
    }  
  }
  return -1;
}


